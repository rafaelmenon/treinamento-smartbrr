import React, { Component } from 'react'
import './Contador.css'
import ContadorCard from './ContadorCard';





class Contador extends Component {
    
    constructor(props) {
      super(props);

    }

    render() {
      
        return (
    
        <div className="container-fluid contador-perso">
        <div className="row">
            <div class="col-md-12 col-xs-12">
                <h2>Comodidade e segurança, para você cliente!</h2>
            </div>
            <ContadorCard class='col-md-4 col-xs-12' title='Mais de' numero='2000' end={3} icon='fa fa-user' subTitle='Clientes atendidos'/>
            <ContadorCard class='col-md-4 col-xs-12' title='Mais de' numero='100' end={3} icon='fa fa-handshake-o' subTitle='Especialitas em Negócios'/>
            <ContadorCard class='col-md-4 col-xs-12' title='Mais de' numero='8000' end={3} icon='fa fa-product-hunt' subTitle='Produtos Nacionais e Importados'/>
            
        </div>
    </div>
   
        )}
        
}

export default Contador