import React, { Component } from 'react'
import './Topic.css'
import TopicCard from './TopicCard'


class Topic extends Component {


    constructor(props){
        super(props)

    }


    render() {
        return (
            <div className="container-fluid topic-perso">
            <div className="row">
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-flag" texto="22 anos realizando sonhos"/>
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-truck" texto="Produtos chegam com total segurança e em excelentes condições"/>
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-credit-card" texto="Condições exclusivas de parcelamento e as melhores taxas"/>
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-life-ring" texto="Suporte Técnico especializado"/>
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-graduation-cap" texto="Cursos e treinamentos em instalações prórpias"/>
                <TopicCard class="col-md-2 col-xs-12" icon="fa fa-archive" texto="Produtos a pronto-entrega"/>
            </div>
        </div>
    
        )
    }
}

export default Topic
