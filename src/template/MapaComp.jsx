import React from 'react'
import propTypes from 'prop-types'


class MapaComp extends React.Component {

    constructor(props) {
        super(props)
    }


    render() {
        return(
         
            <div class="col-md-4 col-xs-12">
                <i id="location" class="fa fa-map-marker location-perso" onClick={() => this.props.localizarUsuario()}>&nbsp;&nbsp;Usar minha localização atual</i>
                               
                <div class="accordion" id="accordionExample"></div>
                {
                    this.props.lista.map(franquia => (                                
                    <div key={franquia.uf} class="card">
                        <div class="card-header" id={"heading"+franquia.uf}>
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target={"#"+franquia.uf} aria-expanded="true" aria-controls={franquia.uf}>
                                {franquia.estado}
                                </button>
                                <i class="fa fa-caret-down icon-perso" data-toggle="collapse" data-target={"#"+franquia.uf} aria-expanded="true" aria-controls={franquia.uf}></i>
                                </h2>                                    
                        </div>
                        <div id={franquia.uf} class="collapse" aria-labelledby={"heading"+franquia.uf} data-parent="#accordionExample">
                            <div class="card-body">
                                {franquia.franquia.map(loja => <p key={loja.nome} class="text-success" onClick={(ev) => this.props.teste(loja.lat, loja.lng)}>{loja.nome}</p> )}             
                            </div>
                        </div>
                    </div>                                
                    ))
                 }
            </div>
            )
        }
}




export default MapaComp