import React, { Component } from 'react'
import CountUp from 'react-countup'
import propTypes from 'prop-types'


class ContadorCard extends Component {
    
    constructor(props) {
      super(props);

    }

    render() {
      
        return (
            <div class={this.props.class}>
                <i class={this.props.icon}></i>
                <p>{this.props.title}</p>
                <CountUp
                    end={this.props.numero}
                    duration={3}
                />
                <p>{this.props.subTitle}</p>
            </div>
        )}
}

ContadorCard.propTypes = {
    class: propTypes.string.isRequired,
    icon: propTypes.string.isRequired,
    title: propTypes.string.isRequired
}


export default ContadorCard