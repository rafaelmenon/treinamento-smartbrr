import React, { Component } from 'react'
import './Sobre.css'
import SobreComp from './SobreComp'



class Sobre extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return(
            <div className="container-fluid sobre-perso">
                <div className="row">
                    <SobreComp texto="ha 22 anos os profissionais da saúde podem contar com equipamentos
                            e produtos de alta qualidade e tecnologia de ponta. Consolidada como
                            a maior rede de lojas de saúde e bem-estar do Brasil, o ISP Saúde
                            destaca-se pelo seu amplo portfólio de produtos que atende aos mais
                            altos padrões de qualidade. Atende as áreas de Fisioterapia, Estética,
                            Fitness, Pilates, Medicina, e Bem-estar, composto por mais de 8 mil itens,
                            das melhores marcas do mercado nacional e internacional. Além da empresa
                            dispor de especialistas habilitados para orientar os profissionais, quanto
                            aos procedimentos, bem como a maneira de usar os equipamentos." 
                            texto2="Toda essa Assistência e comodidade, os profissionais recebem do ISP Saúde,
                            que tem como objetivo oferecer o que há de mais seguro e moderno, para garantir
                            os melhores resultados em cada procedimento."/>         
                </div>
    </div>
        )
    }
}


export default Sobre
    