import React, { Component } from 'react'
import './Header.css'


class Header extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (
            <React.Fragment>
                <div class="container-fluid">
                    <div className="row">
                        <div class="video-header">
                            <div class="embed-responsive embed-responsive-21by9">
                                <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                                <source src="https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4" type="video/mp4"/>
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Header