import React, { Component } from 'react'
import propTypes from 'prop-types'




class TopicCard extends Component {
    
    constructor(props) {
      super(props);
      }



    render() {
      
        return (
            <div class={this.props.class}>
                <i class={this.props.icon}></i>
                <p>{this.props.texto}</p>
            </div>
        )}
}

TopicCard.defaultProps = {
    class: "col-md-2 col-xs-12",
    icon: "fa fa-superpowers",
    texto: "Aqui está falando você definir seu texto"

}


TopicCard.propTypes = {
    class: propTypes.string.isRequired,
    icon: propTypes.string.isRequired,
    texto: propTypes.string.isRequired
}

export default TopicCard