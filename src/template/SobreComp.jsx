import React from 'react'
import propTypes from 'prop-types'


class SobreComp extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <div class={this.props.colunaTitle}>
                    <h3>{this.props.title}</h3>
                </div>
                <div class={this.props.colunaParagrafo}>
                    <p>{this.props.texto}</p>
                    <p>{this.props.texto2}</p>
                </div>
            </React.Fragment>
        )}
}


SobreComp.defaultProps = {
    colunaTitle: "col-md-4 col-xs-12",
    title: "São 20 lojas em todo o Brasil para estar cada vez mais perto de você!",
    colunaParagrafo: "col-md-8 col-xs-12",
    Texto: "Aqui está faltando um texto sobre!"
}


SobreComp.propTypes = {
    colunaTitle: propTypes.string.isRequired,
    title: propTypes.string.isRequired,
    colunaParagrafo: propTypes.string.isRequired,
    texto: propTypes.string.isRequired,
    texto2: propTypes.string.isRequired
}

export default SobreComp