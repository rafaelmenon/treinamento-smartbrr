import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'font-awesome/css/font-awesome.min.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Header from './template/Header'
import Sobre from './template/Sobre'
import Contador from './template/Contador'
import Topic from './template/Topic'
import Map from './template/Mapa'
import Footer from './template/Footer'


export default props => 
    <React.Fragment>
        <Header />
        <Sobre />
        <Contador />
        <Topic />
        <Map />
        <Footer />
    </React.Fragment>