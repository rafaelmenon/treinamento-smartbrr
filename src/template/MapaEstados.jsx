import React from 'react'


class MapaEstados extends React.Component {

    constructor(props) {
        super(props);
    }


    render () {
        return (
            <ul id="map">
                <div id="teste">
                    {
                        this.props.lista.map((franquia, i) => (
                        <li key={i} id={'c'+franquia.uf} estado={franquia.uf}><a id={franquia.uf} title={franquia.uf} onClick={() => this.props.handleClick(franquia.uf)}></a></li>
                        ))      
                    }                        
                </div>
            </ul>
        )
    }

}


export default MapaEstados

