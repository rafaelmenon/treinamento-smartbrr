import React, { Component } from 'react'
import {Map, GoogleApiWrapper} from 'google-maps-react';



    

export class MapContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { google, lat, lng } = this.props;
      return (
          <div className="container-fluid">
            <div className="row">
                <div class="teste-map"> 
                    <Map google={google} 
                        style={{width: '100%', height: '35%'}}
                        center= {{
                            lat: lat,
                            lng: lng
                        }}
                        zoom={17}
                        onClick={this.onMarkerClick}>
                    </Map>
                </div>
            </div>
        </div>
      );
    }
  }
   
  export default GoogleApiWrapper({
  })(MapContainer)