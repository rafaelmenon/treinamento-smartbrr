import React, { Component } from 'react'
import './Map.css'
import $ from 'jquery'
import MapContainer from './testeMapa'
import MapaComp from './MapaComp';
import MapaEstados from './MapaEstados'




class Mapa extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.localizarUsuario = this.localizarUsuario.bind(this);
    this.teste = this.teste.bind(this)

   


    this.state = { 
        lat: 0,
        lng: 0,
        lista: [
            {
                estado: 'Minas Gerais',
                uf: 'mg',
                franquia: [
                    {
                        nome: 'Belo Horizonte/MG',
                        link: '/lojas/belohorizonte1',
                        lat: -19.929518,
                        lng: -43.921741
                    },
                   
                ]
            },
            {
                estado: 'São Paulo',
                uf: 'sp',
                franquia: [
                    {
                        nome: 'ABC/SP',
                        link: '/lojas/abc-sp',
                        lat: -23.717949,
                        lng: -46.5467
                    },
                    {
                        nome: 'Bauru/SP',
                        link: '/lojas/bauru-sp',
                        lat: -22.351808,
                        lng: -49.051224
                    },
                    {
                        nome: 'Campinas/SP',
                        link: '/lojas/campinas-sp',
                        lat: -22.881739,
                        lng: -47.055091
                    },
                    {
                        nome: 'Ribeirão Preto/SP',
                        link: '/lojas/ribeirao-sp',
                        lat: -21.18292,
                        lng: -47.80312
                    },
                    {
                        nome: 'Pacaembu/SP',
                        link: '/lojas/pacaembu-sp',
                        lat: -23.535097,
                        lng: -46.662289
                    },
                    {
                        nome: 'Vila Mariana/SP',
                        link: '/lojas/vilamariana-sp',
                        lat: -23.589322,
                        lng: -46.631488
                    },
                ]
            },          
            {
                estado: 'Mato Grosso do Sul',
                uf: 'ms',
                franquia: [
                    {
                        nome: 'Campo Grande/MS',
                        link: '/lojas/campogrande-ms',
                        lat: -20.46039,
                        lng: -54.610054
                    }
                ]
            },
            {
                estado: 'Paraná',
                uf: 'pr',
                franquia: [
                    {
                        nome: 'Cascavel/PR',
                        link: '/lojas/cascavel-pr',
                        lat: -24.9549234,
                        lng: -53.4543213
                    },
                    {
                        nome: 'Curitiba/PR',
                        link: '/lojas/curitiba-pr',
                        lat: -25.445092,
                        lng: -49.266957
                    },
                    {
                        nome: 'Guarapuava/PR',
                        link: '/lojas/guarapuava-pr',
                        lat: -25.403768,
                        lng: -51.466782
                    },
                    {
                        nome: 'Maringa/PR',
                        link: '/lojas/maringa-pr',
                        lat: -23.424422,
                        lng: -51.92431
                    },
                ]
            },
            {
                estado: 'Santa Catarina',
                uf: 'sc',
                franquia: [
                    {
                        nome: 'Florianópolis/SC',
                        link: '/lojas/florianopolis-sc',
                        lat: -27.5912584,
                        lng: -48.5758632
                    },
                ]
            },
            {
                estado: 'Goiás',
                uf: 'go',
                franquia: [
                    {
                        nome: 'Goiânia/GO',
                        link: '/lojas/goiania-go',
                        lat: -17.4541335,
                        lng: -52.7631391
                    },
                ]
            },
            {
                estado: 'Paraíba',
                uf: 'pb',
                franquia: [
                    {
                        nome: 'João Pessoa/PB',
                        link: '/lojas/joaopessoa-pb',
                        lat: -7.111958,
                        lng: -34.830099
                    },
                ]
            },
            {
                estado: 'Rio Grande do Sul',
                uf: 'rs',
                franquia: [
                    {
                        nome: 'Porto Alegre/RS',
                        link: '/lojas/portoalegre-rs',
                        lat: -30.04069,
                        lng: -51.194675
                    },
                ]
            },
            {
                estado: 'Pernambuco',
                uf: 'pe',
                franquia: [
                    {
                        nome: 'Recife/PE',
                        link: '/lojas/recife-pe',
                        lat: -8.050477,
                        lng: -34.891107
                    },
                ]
            },
            {
                estado: 'Rio de Janeiro',
                uf: 'rj',
                franquia: [
                    {
                        nome: 'Rio de Janeiro/RJ',
                        link: '/lojas/riodejaneiro-rj',
                        lat: -22.923636,
                        lng: -43.229612
                    },
                ]
            },
            {
                estado: 'Bahia',
                uf: 'ba',
                franquia: [
                    {
                        nome: 'Salvador/BA',
                        link: '/lojas/salvador-ba',
                        lat: -13.005161,
                        lng: -38.485591
                    },
                ]
            },
            {
                estado: 'Espírito Santo',
                uf: 'es',
                franquia: [
                    {
                        nome: 'Vitória/ES',
                        link: '/lojas/vitoria-es',
                        lat: -20.303786,
                        lng: -40.298293
                    },
                ]
            }
        ]
    }
  }

  handleClick = (uf) => {
    $('#'+uf).fadeToggle('fast');
  }

  localizarUsuario = () => {
    if (window.navigator && window.navigator.geolocation) {
     var geolocation = window.navigator.geolocation;
     geolocation.getCurrentPosition(sucesso, erro);
    } else {
       alert('Geolocalização não suportada em seu navegador.')
    }
    function sucesso(posicao){
      var latitude = posicao.coords.latitude;
      var longitude = posicao.coords.longitude;
      alert('Sua latitude estimada é: ' + latitude + ' e longitude: ' + longitude )
    }
    function erro(error){
      alert(error)
    }
  }


  teste = (lat, lng) => {
    this.setState({ ...this.state, lat, lng })
    console.log(this.state)
  }
    
    

    render() {
      return (
        <React.Fragment>
            <div id="teste" className="container-fluid map-perso">
                <div className="row">
                    <MapaComp lista={this.state.lista}  localizarUsuario={this.localizarUsuario} teste={this.teste}/>
                    <div class="col-md-8 col-xs-12 perso" align="center">
                        <h3>ENCONTRE A LOJA MAIS PRÓXIMA DE VOCÊ!</h3>
                        <MapaEstados lista={this.state.lista} handleClick={this.handleClick} />
                    </div>
                </div>         
            </div>
            <div class="dMapa">      
                <MapContainer lat = {this.state.lat} lng = {this.state.lng} />
            </div>
        </React.Fragment>
    ) } 
}



export default Mapa;

